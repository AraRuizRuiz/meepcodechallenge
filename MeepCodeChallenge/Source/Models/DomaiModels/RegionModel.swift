//
//  RegionModel.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 22/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import Foundation

enum Region: String, CaseIterable, Codable {
    case madrid
    case malaga
    case lisbon
    case valencia
    
    func getLocation() -> (lat: Double, long: Double) {
        switch self {
        case .madrid:
            return (lat: 40.4167, long: -3.70325)
        case .malaga:
            return (lat: 36.7201600, long: -4.4203400)
        case .lisbon:
            return (lat: 38.7071, long: -9.13549)
        case .valencia:
            return (lat: 39.4697500, long: -0.3773900)
        }
    }
    
    func getName() -> String {
        return Language.translate(key: self.rawValue)
    }
    
    static func isValidRegion(_ regionName: String) -> Bool {
        return getAllRegionsName().contains(regionName)
    }
    
    static func getAllRegionsName() -> [String] {
        var regions: [String] = []
        for region in Region.allCases {
            regions.append(region.getName().simpleString)
        }
        return regions
    }
    
    
    
    
}
