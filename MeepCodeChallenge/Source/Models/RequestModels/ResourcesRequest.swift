//
//  ResourcesRequest.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 21/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import Foundation

struct ResourcesData {
    
    let region: String
    let lowerLeftCoordinate: String
    let upperRightCoordinate: String
    
}

struct ResourcesRequest: APIRequest {
            
    typealias Response = ResourceResponse.Resources
    
    let resourcesRequestData: ResourcesData
    
    init(resourcesRequestData: ResourcesData) {
        self.resourcesRequestData = resourcesRequestData
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var path: String {
        return "/tripplan/api/v1/routers/\(resourcesRequestData.region.lowercased())/resources"
    }
    
    var parameters: [String: String] {
        return ["lowerLeftLatLon": resourcesRequestData.lowerLeftCoordinate,
            "upperRightLatLon": resourcesRequestData.upperRightCoordinate]
    }
    
}
