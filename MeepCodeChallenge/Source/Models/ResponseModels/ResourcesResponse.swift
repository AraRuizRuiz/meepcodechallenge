//
//  ResourcesResponse.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 21/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import Foundation

struct ResourceResponse: Codable {
    typealias Resources = [Resource]
}

struct Resource: Codable {
    let id: String
    let name: String?
    let x: Double
    let y: Double
    let companyZoneID: Int
    let lon, lat: Double?
    let licencePlate: String?
    let range: Int?
    let batteryLevel: Int?
    let seats: Int?
    let model: ResourceModel?
    let pricePerMinuteParking: Double?
    let pricePerMinuteDriving: Double?
    let realTimeData: Bool?
    let engineType: String?
    let resourceType: ResourceType?
    let helmets: Int?
    let station: Bool?
    let availableResources: Int?
    let spacesAvailable: Int?
    let allowDropoff: Bool?
    let bikesAvailable: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, name, x, y
        case companyZoneID = "companyZoneId"
        case lon, lat, licencePlate, range, batteryLevel, seats, model
        case pricePerMinuteParking, pricePerMinuteDriving, realTimeData, engineType, resourceType, helmets, station, availableResources, spacesAvailable, allowDropoff, bikesAvailable
    }
}

enum ResourceModel: String, Codable {
    case modelScooter125 = " Scooter 125"
    case modelScooter125A = "Scooter 125-A"
    case modelScooter125B = "Scooter 125 - B"
    case modelScooter49A = "Scooter 49-A"
    case purpleScooter125A = "Scooter 125 -A"
    case purpleScooter49A = "Scooter 49 -A"
    case scooter125 = "Scooter 125"
    case scooter125A = "Scooter 125 - A"
    case scooter125B = "Scooter 125-B"
    case scooter49A = "Scooter 49 - A"
    case scooter49B = "Scooter 49 -B"
    case scooter125IT = "Scooter 125-IT"
    case scooter125T = "Scooter 125-T"
    case scooter49 = "Scooter 49"
    case scooter49T = "Scooter 49-T"
    case fluffyScooter125B = "Scooter 125-B "
    case purpleScooter125B = "Scooter 125 B "
    case modelScooter125B2 = "Scooter 125 B"
    case citroenBerlingo = "Citroen Berlingo"
    case citroenCZero = "Citroen C-Zero"
    case gogoro2NdEdition = "Gogoro 2nd edition"
    case govecs = "Govecs"
    case kiaNiro = "KIA NIRO"
    case l1 = "L1"
    case l2 = "L2"
    case renaultRENAULTZOE = "Renault RENAULT ZOE"
    case smartSmartForfour3RDGeneration = "smart smart forfour 3rd generation"
    case smartSmartFortwo2NdGenerationElectric = "smart smart fortwo 2nd generation (electric)"
    case v3Rp125T = " v3 . rp 125-T"
    case askoll = "Askoll"
    case bmw1Series = "BMW 1 Series"
    case bmw2Er = "BMW 2er"
    case bmwActiveE = "BMW Active E"
    case bmwI3 = "BMW i3"
    case bmwX1 = "BMW X1"
    case mini = "MINI"
    case mini3Door = "MINI 3-Door"
    case mini5Door = "MINI 5-Door"
    case miniClubman = "MINI Clubman"
    case miniConvertible = "MINI Convertible"
    case renaultZOE = "Renault ZOE" 
}

enum ResourceType: String, Codable {
    case car = "CAR"
    case electricCar = "ELECTRIC_CAR"
    case moped = "MOPED"
    case taxi = "TAXI"
    case motorcycle = "MOTORCYCLE"
}
