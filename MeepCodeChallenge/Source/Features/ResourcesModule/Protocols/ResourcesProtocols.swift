import Foundation
import CoreLocation
// MARK: - View
protocol ResourcesViewProtocol: class {
    
    var presenter: ResourcesPresenterProtocol { get set }
    
    func displayUserLocation()
    func updateLocation(_ location: CLLocation)
    func loadResources(_ resources: [ResourceViewModel])
    func clear()
    func showResourcesNotLoadedError()
    func showVisibleRegionUpdatedError()
}

// MARK: - Presenter
protocol ResourcesPresenterProtocol: class {
    
    var view: ResourcesViewProtocol? { get set }
    var interactor: ResourcesInteractorProtocol { get set }
    var router: ResourcesRouterProtocol { get set }
    
    func onClickRegion(_ region: Region)
    
}

// MARK: - Interactor
protocol ResourcesInteractorProtocol: class {
    
    var presenter: ResourcesInteractorCallbackProtocol?  { get set }
    
    func fetchResources(_ resourcesData: ResourcesData)
}

protocol ResourcesInteractorCallbackProtocol: class {
    
    func fetchedResources(_ result: Result<[Resource], ResourcesError>)
}

// MARK: - Router
protocol ResourcesRouterProtocol: class {
    
}

