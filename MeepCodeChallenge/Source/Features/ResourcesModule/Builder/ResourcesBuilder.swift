import UIKit

class ResourcesBuilder: BuilderProtocol {
    
    func build() -> UIViewController {
        
        let apiClient = APIClient()
        let resourcesRepository = ResourcesRepository(apiClient: apiClient)
        let interactor = ResourcesInteractor(resourcesRepository: resourcesRepository)
        let router = ResourcesRouter()
        let presenter = ResourcesPresenter(with: interactor, router: router)
        let view = ResourcesViewController(with: presenter)
        
        let locationService = LocationService()
        
        presenter.view = view
        presenter.locationService = locationService
        interactor.presenter = presenter
        router.parentVC = view
        
        locationService.delegate = presenter
        
        return view
    }
}

