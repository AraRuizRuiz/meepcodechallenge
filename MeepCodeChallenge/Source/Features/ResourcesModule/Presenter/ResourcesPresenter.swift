import Foundation
import CoreLocation

class ResourcesPresenter {
    
    weak var view: ResourcesViewProtocol?
    var interactor: ResourcesInteractorProtocol
    var router: ResourcesRouterProtocol
    var locationService: LocationServiceProtocol?
    
    var resourcesId = [String]()
    
    init(with interactor: ResourcesInteractorProtocol, router: ResourcesRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    private func clearRegion() {
        resourcesId.removeAll()
    }
    
}

extension ResourcesPresenter: ResourcesPresenterProtocol {
    
    func onClickRegion(_ region: Region) {
        clearRegion()
        self.view?.updateLocation(CLLocation(latitude: region.getLocation().lat, longitude: region.getLocation().long))
    }
    
}

extension ResourcesPresenter: ResourcesInteractorCallbackProtocol {
    
    func fetchedResources(_ result: Result<[Resource], ResourcesError>) {
        switch result {
        case .success(let value):
            self.view?.loadResources(ResourceViewModel.mapperToResourceViewModelList(value.filter { !resourcesId.contains($0.id) }))
        case .failure:
            self.view?.showResourcesNotLoadedError()
        }
    }
    
}

extension ResourcesPresenter: LocationServiceCallback {
    
    func didChangeAuthorization() {
        self.view?.displayUserLocation()
    }
    
    func didUpdateLocation(_ location: CLLocation) {
        self.view?.updateLocation(location)
    }
    
}

extension ResourcesPresenter: MapManagerViewProtocolCallback {
    
    func onVisibleRegionUpdated(region: String, visibleRegion: VisibleRegion) {
        let resourcesData = ResourcesData(region: region,
                                          lowerLeftCoordinate: "\(visibleRegion.lowerLeftCoordinate.latitude),\(visibleRegion.lowerLeftCoordinate.longitude)",
            upperRightCoordinate: "\(visibleRegion.upperRightCoordinate.latitude),\(visibleRegion.upperRightCoordinate.longitude)")
        self.interactor.fetchResources(resourcesData)
    
    }
    
    func onVisibleRegionUpdatedError() {
        self.view?.showVisibleRegionUpdatedError()
    }
    
}
