//
//  ResourceViewModel.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 23/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import UIKit

struct ResourceViewModel {
    let id: String
    let name: String?
    let lat: Double
    let lon: Double
    let companyZoneID: Int
    let color: UIColor
    let licencePlate: String?
    let batteryLevel: Int?
    let seats: Int?
    let model: ResourceModel?
    let pricePerMinuteParking: Double?
    let pricePerMinuteDriving: Double?
    let resourceType: ResourceType?
    let engineType: String?
    let helmets: Int?
    let station: Bool?
    let spacesAvailable: Int?
    let bikesAvailable: Int?
    let snippet: String
    
    static func mapperToResourceViewModelList(_ resources: [Resource]) -> [ResourceViewModel] {
        return resources.map {
            ResourceViewModel.mapperToResourceViewModel($0)
        }
    }
    
    static func mapperToResourceViewModel(_ resource: Resource) -> ResourceViewModel {
        return ResourceViewModel(id: resource.id,
                                 name: resource.name,
                                 lat: resource.y,
                                 lon: resource.x,
                                 companyZoneID: resource.companyZoneID,
                                 color: ColorGenerator.sharedInstance.getColorForId(resource.companyZoneID),
                                 licencePlate: resource.licencePlate,
                                 batteryLevel: resource.batteryLevel,
                                 seats: resource.seats,
                                 model: resource.model,
                                 pricePerMinuteParking: resource.pricePerMinuteParking,
                                 pricePerMinuteDriving: resource.pricePerMinuteDriving,
                                 resourceType: resource.resourceType,
                                 engineType: resource.engineType,
                                 helmets: resource.helmets,
                                 station: resource.station,
                                 spacesAvailable: resource.spacesAvailable,
                                 bikesAvailable: resource.bikesAvailable,
                                 snippet: generateSnippet(resource))
        
    }
    
    static func generateSnippet(_ resource: Resource) -> String {
        var snippet = ""
        if let resourceType = resource.resourceType {
            snippet.append("Type: \(resourceType.rawValue)")
        }
        if let model = resource.model {
            snippet.append("\nModel: \(model)")
        }
        if let engineType = resource.engineType {
            snippet.append("\nEngineType: \(engineType)")
        }
        if let licencePlate = resource.licencePlate {
            snippet.append("\nLicencePlate: \(licencePlate)")
        }
        if let seats = resource.seats {
            snippet.append("\nSeats:\(seats)")
        }
        if let helmets = resource.helmets {
            snippet.append("\nHelmets: \(helmets)")
        }
        if let priceParking = resource.pricePerMinuteParking {
            snippet.append("\nParking Price (per minute): \(priceParking)")
        }
        if let priceDriving = resource.pricePerMinuteDriving {
            snippet.append("\nDriving Price (per minute): \(priceDriving)")
        }
        if let batteryLevel = resource.batteryLevel {
            snippet.append("\nBattery level: \(batteryLevel)%")
        }
        if let bikesAvailable = resource.bikesAvailable {
            snippet.append("\nBikes Available: \(bikesAvailable)")
        }
        
        return snippet
    }

    
}
