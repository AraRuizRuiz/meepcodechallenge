import UIKit
import CoreLocation

class ResourcesViewController: BaseViewController {
        
    @IBOutlet weak var mapManagerView: MapManagerView! {
        didSet {
            guard let presenter = self.presenter as? MapManagerViewProtocolCallback else { return }
            self.mapManagerView.mapManagerDelegate = presenter
        }
    }
    
    @IBOutlet weak var regionSelectorView: RegionSelectorView!
    
    var presenter: ResourcesPresenterProtocol
    
    init(with presenter: ResourcesPresenterProtocol) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    private func setUp() {
        regionSelectorView.delegate = self
    }
    
}

extension ResourcesViewController: ResourcesViewProtocol {
    
    func displayUserLocation() {
        mapManagerView.isMyLocationEnabled = true
    }
    
    func updateLocation(_ location: CLLocation) {
        mapManagerView.updateLocation(location)
    }
    
    func loadResources(_ resources: [ResourceViewModel]) {
        DispatchQueue.main.async {
            self.mapManagerView.loadResources(resources)
        }
    }
    
    func clear() {
        mapManagerView.clearMapView()
    }
    
    func showResourcesNotLoadedError() {
        showError(message: Language.translate(key: "resouresNotLoadedError"))
    }
    
    func showVisibleRegionUpdatedError() {
        showError(message: Language.translate(key: "regionNotUpdatedError"))
    }
    
}

extension ResourcesViewController: RegionSelectorViewDelegate {
    
    func didSelectRegion(_ region: Region) {
        presenter.onClickRegion(region)
    }
    
}



