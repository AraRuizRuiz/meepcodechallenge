import Foundation

class ResourcesInteractor {
    
    weak var presenter: ResourcesInteractorCallbackProtocol?
    let resourcesRepository: ResourcesRepositoryProtocol
    
    init(resourcesRepository: ResourcesRepositoryProtocol) {
        self.resourcesRepository = resourcesRepository
    }
    
}

extension ResourcesInteractor: ResourcesInteractorProtocol {
    
    func fetchResources(_ resourcesData: ResourcesData) {
        resourcesRepository.getResources(resourcesData: resourcesData) { result in
            DispatchQueue.main.async {
                self.presenter?.fetchedResources(result)
            }
        }
    }
    
    
}

