//
//  APIClient.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 21/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import Foundation

final class APIClient {
    
    lazy var session: URLSession = {
        //If you need custom configuration of url session you can do it here or create a HTTPClient builder to inject it.
        return URLSession.shared
    }()
    
    func main<T: APIRequest>(request: T, completion: @escaping((Result<T.Response, Error>) -> Void)) {
        do {
            session.dataTask(with: try request.urlRequest()) { (data, response, error) in
                if let httpResponse = response as? HTTPURLResponse {
                    switch httpResponse.statusCode {
                    case 200...299:
                        if let data = data {
                            do {
                                let model = try JSONDecoder().decode(T.Response.self, from: data)
                                completion(.success(model))
                            } catch {
                                completion(.failure(error))
                            }
                        } else {
                            completion(.failure(APIClientError.emptyDataError))
                        }
                    default:
                        completion(Result.failure(APIClientError.connectionError))
                    }
                } else if error != nil {
                    completion(.failure(APIClientError.connectionError))
                }
            }.resume()
        } catch let error {
            completion(.failure(error))
        }
        
    }
    
}

enum APIClientError: Error {
    case connectionError
    case emptyDataError
}

