//
//  APIRequest.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 21/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import Foundation

let APIURL = "https://apidev.meep.me"


enum HTTPMethod: String {
    case get = "GET"
}

protocol APIRequest {
    
    associatedtype Response: Codable
    var baseURL: URL { get }
    var path: String { get }
    var parameters: [String: String] { get }
    var method: HTTPMethod { get }
    
}

extension APIRequest {
    
    var baseURL: URL {
        guard let baseURL = URL(string: APIURL) else {
            fatalError("Imposible get baseURL for API")
        }
        return baseURL
    }
    
    func urlRequest() throws -> URLRequest {
        
        let URL = baseURL.appendingPathComponent(path)
        
        guard var components = URLComponents(url: URL, resolvingAgainstBaseURL: false) else {
            throw RequestError.createURLError
        }
        
        if !parameters.isEmpty {
            components.queryItems = parameters.map {
                URLQueryItem(name: $0, value: $1 )
            }
        }
        
        guard let finalURL = components.url else {
            fatalError("Unable to retrieve final URL")
        }
        
        var request = URLRequest(url: finalURL)
        request.httpMethod = method.rawValue
        
        
        return request
    }
}

enum RequestError: Error {
    case createURLError
}
