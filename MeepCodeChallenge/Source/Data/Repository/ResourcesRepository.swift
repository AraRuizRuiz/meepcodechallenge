//
//  ResourcesRepository.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 22/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import Foundation

enum ResourcesError: Error {
    case unknownError
}

protocol ResourcesRepositoryProtocol {
    func getResources(resourcesData: ResourcesData, completion: @escaping(Result<[Resource], ResourcesError>) -> Void)
}

class ResourcesRepository: ResourcesRepositoryProtocol {
    
    let apiClient: APIClient
    
    init(apiClient: APIClient) {
        self.apiClient = apiClient
    }
    
    func getResources(resourcesData: ResourcesData, completion: @escaping (Result<[Resource], ResourcesError>) -> Void) {
        let request = ResourcesRequest(resourcesRequestData: resourcesData)
        
        apiClient.main(request: request) { result in
            switch result {
            case .success(let value):
                completion(.success(value))
            case .failure:
                completion(.failure(ResourcesError.unknownError))
            }
        }
    }
    
    
}
