//
//  LocationService.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 22/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationServiceCallback: class {
    func didChangeAuthorization()
    func didUpdateLocation(_ location: CLLocation)
}

protocol LocationServiceProtocol {
    var delegate: LocationServiceCallback? { get }
    var locationManager: CLLocationManager { get }

}

class LocationService: NSObject, LocationServiceProtocol {
    
    internal let locationManager: CLLocationManager
    weak var delegate: LocationServiceCallback?
    
    init(locationManager: CLLocationManager = CLLocationManager()) {
        self.locationManager = locationManager
        super.init()
        startLocation()
    }
    
    private func startLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
    }
}

extension LocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse else { return }
        locationManager.startUpdatingLocation()
        delegate?.didChangeAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        guard let location = locations.first else { return }
        delegate?.didUpdateLocation(location)
    }
    
    
}
