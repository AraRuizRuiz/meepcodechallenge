//
//  ColorGenerator.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 23/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import UIKit

class ColorGenerator {
    
    static let sharedInstance = ColorGenerator()
    var colors = [Int: UIColor]()
    
    func getColorForId(_ id: Int) -> UIColor {
        if let color = colors[id] {
            return color
        } else {
            return generateRandomColorForId(id)
        }
    }
    
    private func generateRandomColorForId(_ id: Int) -> UIColor {
        let newColor = UIColor.random
        if !colors.values.contains(newColor) {
            colors[id] = newColor
            return newColor
        } else {
            return generateRandomColorForId(id)
        }
    }
    
}
