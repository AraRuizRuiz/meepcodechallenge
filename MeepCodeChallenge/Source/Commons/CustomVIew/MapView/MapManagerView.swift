import UIKit
import GoogleMaps

enum MapManagerViewError: Error {
    case regionNotFoundError
}

protocol MapManagerViewProtocolCallback: class {
    typealias VisibleRegion = (lowerLeftCoordinate: CLLocationCoordinate2D, upperRightCoordinate: CLLocationCoordinate2D)
    
    func onVisibleRegionUpdated(region: String, visibleRegion: VisibleRegion)
    func onVisibleRegionUpdatedError()
}

protocol MapManagerViewProtocol {
    
    var isMyLocationEnabled: Bool { get }
    var mapManagerDelegate: MapManagerViewProtocolCallback?  { get set }
    
    func clearMapView()
    func updateLocation(_ location: CLLocation)
    func loadResources(_ resources: [ResourceViewModel])
}

class MapManagerView: UIView, XIBInstantiatableProtocol {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var isMyLocationEnabled: Bool = false {
        didSet {
            mapView.isMyLocationEnabled = isMyLocationEnabled
            mapView.settings.myLocationButton = true
        }
    }
    var visibleMarkers = [GMSMarker]()
    var markersImages = [UIColor: UIImage]()
    weak var mapManagerDelegate: MapManagerViewProtocolCallback?
    
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        instantiate()
        setUp()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        instantiate()
        setUp()
    }
    
    private func setUp() {
        mapView.delegate = self
    }
    
    private func removeNotVisibleResources() {
        visibleMarkers.forEach { marker in
            if !mapView.projection.contains(marker.position) {
                marker.map = nil
            }
        }
        visibleMarkers.removeAll(where: { !mapView.projection.contains($0.position) })
    }
    
    private func createMarkerImage(_ color: UIColor) -> UIImage {
        if let imageForColor = markersImages[color] {
            return imageForColor
        } else {
            let imageForColor = GMSMarker.markerImage(with: color)
            markersImages[color] = imageForColor
            return imageForColor
        }
    }
    
    private func fetchRegionForCenter(center: CLLocationCoordinate2D) {
        GMSGeocoder().reverseGeocodeCoordinate(center) { (response, error) in
            guard let address = response?.firstResult(), let region = address.locality else {
                self.mapManagerDelegate?.onVisibleRegionUpdatedError()
                return
            }
            if Region.isValidRegion(region.simpleString) {
                self.getVisibleRegion(region.simpleString)
            }
        }
    }
    
    private func getVisibleRegion(_ region: String) {
        let visibleRegion = mapView.projection.visibleRegion()
        self.mapManagerDelegate?.onVisibleRegionUpdated(region: region, visibleRegion: (lowerLeftCoordinate: CLLocationCoordinate2D(latitude: visibleRegion.nearLeft.latitude,
                                                                                                                          longitude: visibleRegion.nearLeft.longitude),
                                                                              upperRightCoordinate: CLLocationCoordinate2D(latitude: visibleRegion.farRight.latitude,
                                                                                                                           longitude: visibleRegion.farRight.longitude)))
    }
    
}

extension MapManagerView: MapManagerViewProtocol {
    
    func clearMapView() {
        mapView.clear()
        visibleMarkers.removeAll()
        markersImages.removeAll()
    }
    
    func updateLocation(_ location: CLLocation) {
        mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
    }
    
    func loadResources(_ resources: [ResourceViewModel]) {
        self.removeNotVisibleResources()
        resources.forEach { resource in
            let position = CLLocationCoordinate2D(latitude: resource.lat, longitude: resource.lon)
            let marker = GMSMarker(position: position)
            marker.title = resource.name
            marker.icon = self.createMarkerImage(resource.color)
            marker.snippet = resource.snippet
            DispatchQueue.main.async {
                marker.map = self.mapView
            }
            self.visibleMarkers.append(marker)
        }
    }
    
}

extension MapManagerView: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        fetchRegionForCenter(center: mapView.projection.coordinate(for: mapView.center))
    }
    
}
