import UIKit

protocol RegionSelectorViewDelegate: class {
    func didSelectRegion(_ region: Region)
}

class RegionSelectorView: UIView, XIBInstantiatableProtocol {
    
    @IBOutlet weak var btnChooseRegion: UIButton!
    @IBOutlet weak var regionsView: UIStackView!
    
    weak var delegate: RegionSelectorViewDelegate?
    
    let regionHeight: CGFloat = 50
    var isVisibleRegions = false {
        didSet {
            regionsView.arrangedSubviews.forEach { regionView in
                regionView.isHidden = !isVisibleRegions
            }
        }
    }
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        instantiate()
        setUp()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        instantiate()
        setUp()
    }
    
    private func setUp() {
        backgroundColor = UIColor.white.withAlphaComponent(0.8)
        btnChooseRegion.setTitle(Language.translate(key: "chooseRegion"), for: .normal)
        btnChooseRegion.backgroundColor = .aquaBlue
        btnChooseRegion.layer.cornerRadius = btnChooseRegion.frame.height / 2
        btnChooseRegion.titleLabel?.font = UIFont.mainFont(size: .sixteen, fontType: .book)
        setRegions()
    }
    
    private func setRegions() {
        Region.allCases.forEach { region in
            let regionView = RegionView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            regionView.setRegion(region, delegate: self)
            regionView.heightAnchor.constraint(equalToConstant: regionHeight).isActive = true
            regionsView.addArrangedSubview(regionView)
            regionView.isHidden = true
        }
    }
    
    @IBAction func selectRegion(_ sender: UIButton) {
        isVisibleRegions = !isVisibleRegions
    }
    
}

extension RegionSelectorView: RegionViewDelegate {
    
    func didSelectRegion(_ region: Region) {
        delegate?.didSelectRegion(region)
        isVisibleRegions = !isVisibleRegions
    }
    
}
