import UIKit

protocol RegionViewDelegate: class {
    func didSelectRegion(_ region: Region)
}

class RegionView: UIView, XIBInstantiatableProtocol {
    
    @IBOutlet weak var btnRegion: UIButton!
    @IBOutlet weak var separatorView: UIView!
    weak var delegate: RegionViewDelegate?

    var region: Region? {
        didSet {
            guard let region = self.region else { return }
            btnRegion.setTitle(region.getName(), for: .normal)
        }
    }
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        instantiate()
        setUp()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        instantiate()
        setUp()
    }
    
    private func setUp() {
        btnRegion.backgroundColor = .clear
        btnRegion.titleLabel?.font = UIFont.mainFont(size: .fourteen, fontType: .book)
        separatorView.backgroundColor = UIColor.lightGray.withAlphaComponent(0.8)
    }
    
    func setRegion(_ region: Region, delegate: RegionViewDelegate) {
        self.region = region
        self.delegate = delegate
    }
    
    @IBAction func selectRegion(_ sender: UIButton) {
        guard let region = region else { return }
        delegate?.didSelectRegion(region)
    }
    
}
