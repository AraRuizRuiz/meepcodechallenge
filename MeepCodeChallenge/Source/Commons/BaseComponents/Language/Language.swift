//
//  Language.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 23/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import Foundation

class Language {
    
    class func translate(key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
}
