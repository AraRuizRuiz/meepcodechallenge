//
//  BaseViewController.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 25/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    func showError(message: String) {
        let alert = UIAlertController(title: Language.translate(key: "errorTitle"), message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: Language.translate(key: "ok"), style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}
