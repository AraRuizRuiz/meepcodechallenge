//
//  UIFontExtension.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 23/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import UIKit

enum FontType {
    case bold
    case book
}

enum FontSize: CGFloat {
    case fourteen = 14.0
    case sixteen = 16.0
}

extension UIFont {
    
    class func mainFont(size: FontSize, fontType: FontType) -> UIFont {
        switch fontType {
        case .bold:
            return UIFont.systemFont(ofSize: size.rawValue, weight: .bold)
        case .book:
            return UIFont.systemFont(ofSize: size.rawValue, weight: .regular)
        
        }
    }
}
