//
//  UIColorExtension.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 23/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
    
    static var aquaBlue: UIColor {
        return UIColor(red: 87.0 / 255.0, green: 184.0 / 255.0, blue: 162.0 / 255.0, alpha: 1.0)
    }
}

