//
//  StringExtension.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 23/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import Foundation

extension String {
    
    var simpleString: String {
        let simple = folding(options: [.diacriticInsensitive, .widthInsensitive, .caseInsensitive], locale: nil)
        let nonAlphaNumeric = CharacterSet.alphanumerics.inverted
        return simple.components(separatedBy: nonAlphaNumeric).joined(separator: "").lowercased()
    }
    
}
