//
//  BuilderProtocol.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 22/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import UIKit

protocol BuilderProtocol {
    func build() -> UIViewController
}
