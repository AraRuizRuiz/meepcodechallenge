//
//  AppDelegate.swift
//  MeepCodeChallenge
//
//  Created by Araceli Ruiz Ruiz on 21/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import UIKit
import GoogleMaps

let googleAPIKey = "AIzaSyCpSY3u5rrCIIxBlA7wxw-zEViEEFDq9Wg"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey(googleAPIKey)
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = ResourcesBuilder().build()
        window?.makeKeyAndVisible()
        return true
    }

}

