//
//  ResourcesPresenterTests.swift
//  MeepCodeChallengeTests
//
//  Created by Araceli Ruiz Ruiz on 25/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import XCTest
import CoreLocation
@testable import MeepCodeChallenge


class ResourcesPresenterTests: XCTestCase {
    
    var sut: ResourcesPresenter!
    var view: MockResourcesView!
    var interactor: MockResourcesInteractor!
    var router: MockResourcesRouter!
    var locationService: MockLocationService!
    var mapManagerView: MockMapManagerView!
    
    
    override func setUp() {
        
        interactor = MockResourcesInteractor()
        router = MockResourcesRouter()
        sut = ResourcesPresenter(with: interactor, router: router)
        view = MockResourcesView(presenter: sut)
        locationService = MockLocationService()
        mapManagerView = MockMapManagerView()
        
        interactor.presenter = sut
        sut.view = view
        sut.locationService = locationService
        locationService.delegate = sut
        mapManagerView.mapManagerDelegate = sut
    }
    
    override func tearDown() {
        sut = nil
        interactor = nil
        view = nil
        locationService = nil
        router = nil
    }
    
    private func givenLocationServiceDidChangeAuthorization() {
        locationService.willReturnAuthorizationStatus(status: .authorizedWhenInUse)
    }
    
    private func givenALocationWillBeReturned() {
        locationService.willReturnLocation(location: TestUtils.createFakeLocation())
    }
    
    private func givenAListOfResourcesWillbeReturned() {
        let model = TestUtils.createFakeResourceListResponse()
        interactor.willReturResources(model: model)
    }
    
    private func givenAnUpdatedVisibleRegion() {
        mapManagerView.willReturnRegion(region:TestUtils.createFakeRegion(), visibleRegion: TestUtils.createAFakeVisibleRegion())
    }
    
    private func givenResourcesErrorWillBeReturned() {
        interactor.willReturResources(model: TestUtils.createFakeResourceListError())
    }
    
    func test_display_user_location_when_is_authorized() {
        givenLocationServiceDidChangeAuthorization()
        XCTAssertTrue(self.view.isDisplayUserLocation)
    }
    
    func test_display_location_when_updated() {
        givenALocationWillBeReturned()
        XCTAssertTrue(self.view.location?.coordinate.latitude == 38.7071)
    }
    
    func test_display_resources_when_loaded() {
        givenAListOfResourcesWillbeReturned()
        givenAnUpdatedVisibleRegion()
        XCTAssertTrue(self.view.resources?.count == 118)
        XCTAssertTrue(self.view.resources?.first?.id == "402:11059006")
    }
    
    func test_display_resources_error_when_is_returned() {
        givenResourcesErrorWillBeReturned()
        givenAnUpdatedVisibleRegion()
        XCTAssertTrue(self.view.isShowResourcesNotLoadedError)
    }
    
}

class MockResourcesInteractor: ResourcesInteractorProtocol {
    
    var presenter: ResourcesInteractorCallbackProtocol?
    var resources: Result<[Resource], ResourcesError>?
    
    func willReturResources(model: Result<[Resource], ResourcesError>) {
        self.resources = model
    }
    
    func fetchResources(_ resourcesData: ResourcesData) {
        if let resources = self.resources {
            presenter?.fetchedResources(resources)
        }
    }
}

class MockResourcesView: ResourcesViewProtocol {
    
    var presenter: ResourcesPresenterProtocol
    var isDisplayUserLocation = false
    var location: CLLocation?
    var resources: [ResourceViewModel]?
    var isClear = false
    var isShowResourcesNotLoadedError = false
    var isShowVisibleRegionUpdatedError = false
    
    
    init(presenter: ResourcesPresenterProtocol) {
        self.presenter = presenter
    }
    
    func displayUserLocation() {
        isDisplayUserLocation = true
    }
    
    func updateLocation(_ location: CLLocation) {
        self.location = location
    }
    
    func loadResources(_ resources: [ResourceViewModel]) {
        self.resources = resources
    }
    
    func clear() {
        isClear = true
    }
    
    func showResourcesNotLoadedError() {
        isShowResourcesNotLoadedError = true
    }
    
    func showVisibleRegionUpdatedError() {
        isShowVisibleRegionUpdatedError = true
    }
    
}

class MockLocationService: LocationServiceProtocol {
    var delegate: LocationServiceCallback?
    var locationManager: CLLocationManager
    var location: CLLocation?

    init(locationManager: CLLocationManager = CLLocationManager()) {
        self.locationManager = locationManager
    }
    
    func willReturnAuthorizationStatus(status: CLAuthorizationStatus) {
        delegate?.didChangeAuthorization()
    }
    
    func willReturnLocation(location: CLLocation) {
        delegate?.didUpdateLocation(location)
    }


}

class MockResourcesRouter: ResourcesRouterProtocol {}

class MockMapManagerView: MapManagerViewProtocol {
    
    typealias VisibleRegion = (lowerLeftCoordinate: CLLocationCoordinate2D, upperRightCoordinate: CLLocationCoordinate2D)
    
    var isMyLocationEnabled: Bool = false
    
    var mapManagerDelegate: MapManagerViewProtocolCallback?
    
    func clearMapView() {
        //
    }
    
    func updateLocation(_ location: CLLocation) {
        //
    }
    
    func loadResources(_ resources: [ResourceViewModel]) {
        //
    }
    
    
    func willReturnRegion(region: String, visibleRegion: VisibleRegion) {
        mapManagerDelegate?.onVisibleRegionUpdated(region: region, visibleRegion: visibleRegion)
    }
    
}


