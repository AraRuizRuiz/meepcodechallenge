//
//  TestUtils.swift
//  MeepCodeChallengeTests
//
//  Created by Araceli Ruiz Ruiz on 25/06/2019.
//  Copyright © 2019 Araceli Ruiz Ruiz. All rights reserved.
//

import Foundation
import CoreLocation
@testable import MeepCodeChallenge

final class TestUtils {
    
    static func parseLocalJson<T: APIRequest>(with name: String, and request: T) -> T.Response? {
        do {
            return try Bundle(for: TestUtils.self).url(forResource: name, withExtension: "json")
                .flatMap { try Data(contentsOf: $0) }
                .flatMap { try JSONDecoder().decode(T.Response.self, from: $0)}
        } catch let err {
            print(err)
            return nil
        }
    }
    
    static func createFakeResourceListResponse() -> Result<[Resource], ResourcesError> {
        let request = ResourcesRequest(resourcesRequestData: createFakeResourcesData())
        guard let model = parseLocalJson(with: "ResourceResponse", and: request) else {
            return .failure(ResourcesError.unknownError)
        }
        return .success(model)
    }
    
    static func createFakeResourceListError() -> Result<[Resource], ResourcesError> {
        return .failure(ResourcesError.unknownError)
    }
    
    static func createFakeResourcesData() -> ResourcesData {
        return ResourcesData(region: "lisboa",
                             lowerLeftCoordinate: "38.6936694964188,-9.143536537885666",
                             upperRightCoordinate: "38.72086259348507,-9.12744328379631")
    }
    
    static func createFakeLocation() -> CLLocation {
        return CLLocation(latitude: 38.7071, longitude: -9.13549)
    }
    
    static func createFakeRegion() -> String {
        return "liboa"
    }

    static func createAFakeVisibleRegion() ->  (lowerLeftCoordinate: CLLocationCoordinate2D, upperRightCoordinate: CLLocationCoordinate2D) {
        return (CLLocationCoordinate2D(latitude: 38.6936694964188,
                                       longitude: -9.143536537885666),
                CLLocationCoordinate2D(latitude: 38.72086259348507,
                                       longitude: -9.12744328379631))
    }

}
